# Protractor Chromium headless on Alpine Linux
This image is based on the [official node docker image](https://hub.docker.com/_/node/).

This does not currently work with chromium headless because the official node alpine image is based on Alpine linux 3.6 but Chromium >= version 58 ist available in alpine linux 3.7.
See this [issue](https://github.com/nodejs/docker-node/issues/473) for more details.
