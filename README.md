# Protractor Chrome headless docker image
This project creates a docker image and pushes it to [Dockerhub](https://hub.docker.com/r/beamdev/protractor-chrome-headless/)

To get the image use commands like this:
```
# pull the latest image to the local machine
sudo docker pull beamdev/protractor-chrome-headless:alpine

# start the latest image in interactive mode and remove the container when exiting
sudo docker run -it --name beam-protractor-headless --rm docker.repo.pnet.ch/beamdev/protractor-chrome-headless:alpine

# to check the version try this:
sudo docker run -it --rm docker.repo.pnet.ch/beamdev/protractor-chrome-headless:alpine cat /etc/alpine-release

sudo docker run -it --rm docker.repo.pnet.ch/beamdev/protractor-chrome-headless:alpine chromium-browser --version
```


## Existing Dockerfiles as references
* [Zenika alpine-chrome](https://github.com/Zenika/alpine-chrome)
* [sylvaindumont - docker-node-karma-protractor-chrome](https://github.com/sylvaindumont/docker-node-karma-protractor-chrome/) - use the latest alpine branch for reference
* [jciolek - docker-protractor-headless](https://github.com/jciolek/docker-protractor-headless)
* [hortonworks - docker-e2e-protractor](https://github.com/hortonworks/docker-e2e-protractor)

